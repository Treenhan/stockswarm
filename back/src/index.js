const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

//kafka imports
const kafka = require('kafka-node');
const bf = require('body-parser');
const kafka_config = require('./kafka_config');

// sleep time expects milliseconds
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

// Wait 8s for kafka to create topics
sleep(8000).then(() => {
  try {
    const client = new kafka.KafkaClient({kafkaHost: 'kafka:9092'});
    let consumer = new kafka.Consumer(
      client,
      [{topic: 'catch-front', partitions: 1}],
      {
        autoCommit: true,
        fetchmaxWaitMs: 1000,
        fetchMaxBytes: 1024 * 1024,
        encoding: 'utf8',
        fromOffset: false
      }
    );
    // Pass the message from kafka on to the front
    consumer.on('message', async function(message) {
      console.log('kafka->back', message.value);
      // forward the data from kafka to the front using back-front channel of the websocket
      io.emit('back-front',{ 'value' : message.value});
    });
    consumer.on('error', function(err) {
      console.log('error',err);
    });
  } catch(e) {
    console.log(e);
  }
});

// try listening on 8081 so that the front can forward all requests to the back
http.listen(8081, function(){
  console.log('listening on *:8081');
});

// just a test function to make sure that the back is working fine
app.get('/time', function (req, res) {
    res.send({'time': 123});
})
 
// make API calls on behalf of front to both fetch and catch
app.get('/activate', function(req, res) {
    var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://fetch:8082/start/msft');
    xhr.send();
    console.log('request sent to Fetch');

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://catch:4567/stream/price_change?direction=1&symbol=msft&length=2&input=fetch-catch&output=catch-front');
    xhr.send();
    console.log('request sent to Catch');
})

// This is called when a front is connected
io.on('connection', function (socket) {
    console.log('a user connected');
});