#!/bin/bash

# start zookeeper
bin/zookeeper-server-start.sh -daemon config/zookeeper.properties

#start kafka
bin/kafka-server-start.sh -daemon config/server.properties

sleep 4

# create the "fetch" topic
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic fetch-catch
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic catch-front

# keep the container running
tail -f /dev/null
