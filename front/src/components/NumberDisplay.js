import React, { Component } from 'react';
import '../styles/NumberDisplay.css';
import PropTypes from 'prop-types';

function NumberDisplay(props) {
    // Only display "of xx" when a max prop is available
    let max = null;

    if (props.max !== undefined) {
        max =
            <span className="max">
                of {props.max}
            </span>;
    }
    return (
        <div className="NumberDisplay">
            <span className="value">
                {props.value}
            </span>

            {max}
        </div>
    );

}

NumberDisplay.propsTypes = {
    min: PropTypes.number,
    value: PropTypes.number,
    max: PropTypes.number
}

export default NumberDisplay;