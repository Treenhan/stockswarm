import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Import styling
import '../styles/Progress.css';

function Progress(props) {
    let value = props.value;

    // Limit value to min and max bounds
    if (value < props.min) {
        value = 0;
    } else if (value > props.max) {
        value = props.max;
    }

    // Adjust progress bar using inline styles
    let innerWidthStyle = { width: `${ (value / props.max) * 100 }%` };

    return (
        <div className="Progress">
            <div className="inner" style={innerWidthStyle} />
        </div>
    );
}

// Provide a default value when one isn't supplied
Progress.defaultProps = {
    value: 0
}

// Enforce the type of props to send to this component
Progress.propTypes = {
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
}

export default Progress;