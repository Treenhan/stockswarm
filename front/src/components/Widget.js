import React from 'react';
import '../styles/Widget.css';
import Loading from './Loading.js';
import PropTypes from 'prop-types';

function Widget(props) {
    // Create inline styles to make grid elements span multiple rows/columns
    var spanStyles = {};
    if (props.colspan !== 1) {
        spanStyles.gridColumn = `span ${props.colspan}`;
    }
    if (props.rowspan !== 1) {
        spanStyles.gridRow = `span ${props.rowspan}`;
    }

    return (
        <div style={spanStyles} className="Widget">
            <div className="header">
                <h2>
                    {props.heading}
                </h2>
                {/* Conditionally show the loading spinner */}
                {props.loading ? <Loading /> : ""}
            </div>
            <div className="content">
                {props.children}
            </div>
        </div>
    );
}

Widget.defaultProps = {
    heading: "Unnamed Widget",
    colspan: 1,
    rowspan: 1 
}

Widget.propsTypes = {
    heading: PropTypes.string,
    colspan: PropTypes.number,
    rowspan: PropTypes.number,
    children: PropTypes.element.isRequired
}

export default Widget;