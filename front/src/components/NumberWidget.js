import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Import components
import Widget from '../components/Widget';
import NumberDisplay from '../components/NumberDisplay';
import Progress from '../components/Progress';

//Import styling
import '../styles/NumberWidget.css';
import '../styles/Button.css';

function NumberWidget(props) {
    // Decide whether to show widget
    function showWidget() {
        // Show loading indicator while initial data is being fetched
        if (props.value === undefined) {
            return <p>Loading...</p>;
        }

        return <div className="NumberWidget">
            <NumberDisplay max={props.max} value={props.value} />
            {/* Conditionally show the progress bar */}
            {showProgress()}
        </div>
    }

    // Decide whether to show a progress bar
    function showProgress() {
        // Only show if the required min, max and value props are supplied
        if (props.min !== undefined && props.max !== undefined && props.value !== undefined) {
            return <Progress min={props.min} max={props.max} value={props.value} />;
        }

        return null;
    }

    return (
        // Wrap the number display component in the generic wrapper
        <Widget heading={props.heading} colspan={props.colspan} rowspan={props.rowspan} loading={props.loading}>
            {showWidget()}
        </Widget>
    );
}

// Enforce the type of props to send to this component
NumberWidget.propTypes = {
    heading: PropTypes.string,
    colspan: PropTypes.number,
    rowspan: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    value: PropTypes.number
}

export default NumberWidget;