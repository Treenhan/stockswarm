import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// Import components
import Widget from '../components/Widget';
import '../styles/CountWidget.css';

//Import styling
import '../styles/Button.css';

function CountWidget(props) {
    useEffect(() => {}, [props.value]);
    
    function activate(e) {
        console.log('activate button clicked');
        var xhr = new XMLHttpRequest();

        // get a callback when the server responds
        xhr.addEventListener('load', () => {
            // update the state of the component with the result here
            console.log(xhr.responseText);
        })
        // open the request with the verb and the url
        xhr.open('GET', 'http://localhost:8080/activate');
        // send the request
        xhr.send();
    }

    // Decide whether to show widget
    function showWidget() {
        return (<div className="CountWidget">
            <span className="value">
                {props.value}
            </span>
            <span className="subtext">
                events detected
            </span>
            <button onClick={activate}>
                Activate
            </button>
        </div>
        );
    }

    return (
        // Wrap the number display component in the generic wrapper
        <Widget heading={props.heading} colspan={props.colspan} rowspan={props.rowspan} loading={props.loading}>
            {showWidget()}
        </Widget>
    );
}

// Enforce the type of props to send to this component
CountWidget.propTypes = {
    heading: PropTypes.string,
    colspan: PropTypes.number,
    rowspan: PropTypes.number,
    value: PropTypes.number,
}

export default CountWidget;