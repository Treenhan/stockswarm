import React from 'react';
import {useState, useEffect} from 'react';
import io from 'socket.io-client';
import './App.css';
import Widget from './components/Widget.js';
import CountWidget from './components/CountWidget.js';
import NumberWidget from './components/NumberWidget.js'
const socket = io.connect('http://localhost:8080/');

function App() {
  const [currentTime, setCurrentTime] = useState(0);
  const [widgetValues, setWidgetValues] = useState(0);

// by default popualte data for one first tile
	useEffect(() => {
		fetch('/time').then(res => res.json()).then(data => {setCurrentTime(data.time);});
    socket.on('back-front', (msg) => {
      console.log(msg.value);
      setWidgetValues((prevValue) => prevValue + 1);
      return (()=>{
        socket.close();
      });
    });
	},[]);
 
  return (
    <div className="App">
      <CountWidget colspan={2} rowspan={1} value={widgetValues} heading="[MSFT] Increase Sequence" />
      <NumberWidget colspan={2} rowspan={1} min={1} max={9} value={8} heading="Test" />
      <Widget colspan={1} rowspan={2} />
      <header className="App-header">
        <p>The current time is {currentTime}.</p>
      </header>
    </div>
  );
}

export default App;
