from flask import Flask
from ticker import Ticker
from threading import Thread
import logging
import os

_running_tickers = {}

# initializing Flask and logger
app = Flask('fetch')
app.logger.setLevel(logging.INFO)

# start a ticker
def start_ticker(ticker_id):
    app.logger.info(f'Fetch Activated on {ticker_id}')
    # Previous value of the ticker
    previous = None

    # disable ssl checking
    ssl._create_default_https_context = ssl._create_unverified_context

    # get the ticker
    ticker = yf.Ticker(ticker_id)

    # poll everyminute for the newest data
    while True:
        current = df.to_json(ticker.history(period="1m", interval="1m"))

        # Only send out data if it's different to the previous one
        if current != previous:
            # Append Ticker name to the data
            current_json = json.loads(current)
            current_json.update({'Name':ticker_id})
            current = json.dumps(current_json)
            publish_message(KAFKA_TOPIC, 'stock', current)

        sleep(60)

# Flask API routes
@app.route('/')
def root():
    app.logger.info('Hello logging')
    return "Hello, World!"

# List active fetcher
@app.route('/list')
def list_tickers():
    return f"{list(_running_tickers.keys())}"

# Stop a running ticker
@app.route('/stop/<ticker_id>')
def stop_ticker(ticker_id):
    if ticker_id not in _running_tickers:
        app.logger.info(f"{ticker_id} fetch does not exist")
        return f"{ticker_id} fetch does not exist"

    _running_tickers[ticker_id].terminate()
    del _running_tickers[ticker_id]

    return "terminated"

# Get stock price from a particular company
@app.route('/start/<ticker_id>')
def start_ticker(ticker_id):
    if ticker_id in _running_tickers:
        app.logger.info(f"{ticker_id} fetch already existed")
        return f"{ticker_id} fetch already exists"
        
    ticker = Ticker(ticker_id)
    thread = Thread(target=ticker.run)
    thread.start()

    # Add this ticker to the list of already ran tickers
    _running_tickers[ticker_id] = ticker

    return f'Ticker {ticker_id}: Invoked'

# Start Flask server
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=8082)
