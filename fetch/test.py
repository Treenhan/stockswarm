from unittest.mock import Mock, patch
import main
import datetime
import unittest
import json

# Within working hours date
class WorkingHours(datetime.datetime):
    @classmethod
    def utcnow(cls):
        return cls(2020, 3, 31, 14, 30, 29, 37019)

# Outside working hours date
class NotWorkingHours(datetime.datetime):
    @classmethod
    def utcnow(cls):
        return cls(2020, 3, 31, 20, 30, 29, 37019)

# Set the date to working hours initially
datetime.datetime = WorkingHours

# Load sample data
with open('sample_data.json') as json_file:
    sample_data = json.load(json_file)
sample_data = list(map(lambda x: json.dumps(x), sample_data))

sample_data_counter=-1
def next_sample_data(*args):
    global sample_data_counter

    # This shouldn't be called but just in case
    # TODO return end of day event probably?
    if sample_data_counter >= len(sample_data):
        return json.dumps({})
    
    sample_data_counter += 1
    # If out of sample data, turn the date to weekend
    if sample_data_counter == len(sample_data) - 1:
        datetime.datetime = NotWorkingHours
    return sample_data[sample_data_counter]

class FetchTest(unittest.TestCase):
    @patch('ticker.Ticker.format_data')
    def test_mock_stubs(self,mock_format_data):
        mock_format_data.side_effect = next_sample_data
        # ticker = Mock()
        # requests.get.side_effect = [Timeout, response_mock]
        main.app.run(debug=True,host='0.0.0.0', port=8082)

if __name__ == '__main__': 
    unittest.main() 