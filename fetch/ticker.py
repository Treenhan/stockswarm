from time import sleep
from kafka import KafkaProducer
import ssl
import yfinance as yf
from pandas import DataFrame as df
import json
import logging
import collections
import datetime

# For checking the  working time
WorkingTime = collections.namedtuple('WorkingTime',['start_hour','start_min', 'end_hour','end_min'])
_seconds_between_fetch = 5

class Ticker:
    def __init__(self,ticker_id):
        self.ticker_id = ticker_id
        self.logger = logging.getLogger('fetch.ticker')
        self._running = True
        self._KAFAK_SERVER='kafka:9092'
        self._KAFKA_TOPIC='fetch-catch'
        self._producer = None

        # Working time in US is from 9:30am to 4pm UTC -4
        self._working_time = WorkingTime(13,30,20,0)

        # connect to kafka
        self.connect_kafka_producer()

    def is_US_working_time(self):
        now = datetime.datetime.utcnow()

        # Check within weekday
        if now.isoweekday() not in range(1, 6):
            self.logger.info(f'Ticker {self.ticker_id}: Not in Weekdays')
            return False

        if (now.hour*60 + now.minute) not in range(self._working_time.start_hour*60 + self._working_time.start_min, self._working_time.end_hour*60 + self._working_time.end_min + 1):
            self.logger.info(f'Ticker {self.ticker_id}: Not in Working Hours')
            return False
        
        return True

    def format_data(self,data,symbol,data_type):
        data = json.loads(data)
        result = {}

        if len(data.keys()) != 1:
            self.logger.error(f'Ticker {self.ticker_id}: data is not in the right format')
            return None

        # Append Ticker name to the data
        time_stamp = list(data.keys())[0]

        # Refill the data back in
        for key,value in data[time_stamp].items():
            result[key.lower()] = value

        # Put in timestamp and type
        result['timestamp'] = time_stamp
        result['type'] = data_type
        result['symbol'] = symbol

        result = json.dumps(result)
        return result
        
    
    def get_ticker_id(self):
        return self.ticker_id

    def terminate(self):
        self.logger.info(f'Ticker {self.ticker_id}: Terminating ')
        self._running = False

    def connect_kafka_producer(self):
        try:
            self._producer = KafkaProducer(bootstrap_servers=[self._KAFAK_SERVER], api_version=(0, 10),value_serializer=lambda m: m.encode('ascii'))
            self.logger.info(f'Ticker {self.ticker_id}: Connected to Kafka')
        except Exception as ex:
            self.logger.info(f'Ticker {self.ticker_id}: Exception while connecting Kafka')
            self.logger.info(str(ex))

    # use this method to publish messages to a kafka topic
    def publish_message(self,topic_name, key, value):
        try:
            key_bytes = bytes(key, encoding='utf-8')
            self._producer.send(topic_name, key=key_bytes, value=value)
            self._producer.flush()
            self.logger.info(f'Ticker {self.ticker_id}: Message published successfully: {value}')
        except Exception as ex:
            self.logger.info(f'Ticker {self.ticker_id}: Exception in publishing message')
            self.logger.info(str(ex))

    def run(self):
        self.logger.info(f'Ticker {self.ticker_id}: Started')
        # disable ssl checking
        ssl._create_default_https_context = ssl._create_unverified_context

        # get the ticker
        ticker = yf.Ticker(self.ticker_id)

        # poll everyminute for the newest data
        while self._running:
            # Only fetch data when it is working time
            if self.is_US_working_time():
                ticker_data = df.to_json(ticker.history(period="1m", interval="1m"),orient='index')

                # Reformat the data to the right form
                ticker_data = self.format_data(ticker_data,'msft','quote')

                 # Sends out data if no error
                if ticker_data:
                    self.publish_message(self._KAFKA_TOPIC, 'stock', ticker_data)

            for _ in range(_seconds_between_fetch):
                # Wait for 1 minute but also exit if forced
                sleep(1)
                if not self._running:
                    self.logger.info(f'Ticker {self.ticker_id}: Terminated ')
                    return
