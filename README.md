# StockSwarm [WORK IN PROGRESS]
## For impatient ones:

- Right after cloning this repo, please use `docker-compose build` to build all necessary dockder images. There might be heaps of red warnings from npm, just ignore them :D
- After all docker images are built, use `docker-compose up` to run the entire application, you can access it on the browser on `localhost:8080`.
- Once finished, feel free to hit `Ctrl+C` to stop the application (You might need to press it twice because kafka sometimes does not stop). Also, if you want to start the app next time, make sure you do `docker-compose down; docker-compose up`

## How do I use this?

- Once in the browser, you will see the dashboard with different tiles, each tile represents one rule implemented by EventSwarm. To start streaming data and capture matches, hit the `Activate` button. The tile will count the number of matches that EventSwarm catches during its execution.

## How do I tune the behavior of the appplication?

- By default, the `fetch` component streams the test data defined in `fetch/sample_data.json`. Feel free to change that file if you want to test another dataset. Or if you want to stream real data from yahoo finance, open the `docker-compose.yml` file then change line 42 from `command: python test.py` to `command: python main.py`

## Introduction
![architecture](./architecture.png)
